﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damageable : MonoBehaviour
{
    [SerializeField] private float startHP = 5;
    [SerializeField] private float startMinHP = 0;
    [SerializeField] private float startMaxHP = 5;
    [SerializeField] private bool destroyOnZeroHP;

    private float curHP;
    public float CurHP { get { return curHP; } }
    private float curMinHP;
    public float CurMinHP { get { return curMinHP; } }
    private float curMaxHP;
    public float CurMaxHP { get { return curMaxHP; } }

    private void Start()
    {
        curHP = startHP;
        curMinHP = startMinHP;
        curMaxHP = startMaxHP;
    }

    public void MinHPDelta(float _delta)
    {
        curMinHP += _delta;
    }

    public void MaxHPDelta(float _delta)
    {
        curMaxHP += _delta;
    }

    public void Repair(float _amount)
    {
        CurHPDelta(_amount);
    }

    public void Damage(float _amount)
    {
        CurHPDelta(-_amount);
    }

    void CurHPDelta(float _amount)
    {
        curHP += _amount;
        curHP = Mathf.Clamp(CurHP, CurMinHP, CurMaxHP);

        if (destroyOnZeroHP)
        {
            if (curHP <= 0)
                Destroy(this.gameObject);
        }
            
    }

}
