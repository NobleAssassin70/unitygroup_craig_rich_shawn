﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{
    [SerializeField]
    private Transform player;
    [SerializeField]
    private Vector3 offset;
    [SerializeField]
    private float followSens = 3;

    private float playerSpeed;

    void Start()
    {
        StartCoroutine(CalcPlayerSpeed());
    }

    void FixedUpdate()
    {
        FollowPlayer();
    }

    void FollowPlayer()
    {
        transform.position = Vector3.Lerp(transform.position, player.position + offset, Time.deltaTime * followSens);
    }

    IEnumerator CalcPlayerSpeed()
    {
        while (Application.isPlaying)
        {
            // Position at frame start
            Vector3 lastPos = player.position;
            // Wait till it the end of the frame
            yield return new WaitForFixedUpdate();
            // Calculate velocity: Velocity = DeltaPosition / DeltaTime
            playerSpeed = ((lastPos - player.position) / Time.deltaTime).magnitude;
        }
    }

}
