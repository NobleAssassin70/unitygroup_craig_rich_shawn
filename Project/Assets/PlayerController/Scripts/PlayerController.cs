﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    //movement
    [SerializeField]
    private float speed = 6;
    [SerializeField]
    private float jumpForce = 5;
    private bool jump;

    private float horMovement;
    private float vertMovement;
    [SerializeField]
    private LayerMask groundMask;
    [SerializeField]
    private Vector2 groundBoxSize = Vector2.one;
    [SerializeField]
    private float groundBoxCenter = 0.5f;
    private Vector3 groundPos;
    private bool grounded;


    [SerializeField]
    private bool faceRightOnStart = true;
    private bool moveRight;

    //comps
    private Rigidbody2D rb;
    private Animator anim;

    void Start()
    {
        //get comps
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();

        OrientPlayer();
    }

	// Update is called once per frame
	void Update ()
    {
        GetInputs();
        CheckFlipping();
        CheckGrounded();

        SyncAnimations();
	}

    void FixedUpdate()
    {
        MovePlayer();
    }

    void GetInputs()
    {
        horMovement = Input.GetAxisRaw("Horizontal");

        jump = Input.GetButtonDown("Jump");
        if (jump)
        {
            Jump();
        }
    }

    void MovePlayer()
    {
        Vector3 move = Vector3.forward * horMovement * speed * Time.fixedDeltaTime;
        if (moveRight)
            transform.Translate(move, Space.Self);
        else
            transform.Translate(-move, Space.Self);
    }

    void Jump()
    {
        if (grounded)
        {
            rb.AddForce(Vector3.up * jumpForce, ForceMode2D.Impulse);
        }
    }

    void CheckFlipping()
    {
        if (horMovement < 0)
        {
            if (moveRight)
            {
                FlipPlayer();
                moveRight = false;
            }
        }

        if (horMovement > 0)
        {
            if (!moveRight)
            {
                FlipPlayer();
                moveRight = true;
            }
        }
    }

    void FlipPlayer()
    {
        transform.Rotate(0, 180, 0);
    }

    void CheckGrounded()
    {
        groundPos = new Vector3(transform.position.x, transform.position.y + groundBoxCenter, transform.position.z);
        grounded = Physics2D.OverlapBox(groundPos, groundBoxSize, 0, groundMask);
    }

    void OnDrawGizmosSelected()
    {
        if (!Application.isPlaying)
            return;

        Gizmos.color = Color.cyan;
        Gizmos.DrawWireCube(groundPos, groundBoxSize);
    }

    void OrientPlayer()
    {
        moveRight = faceRightOnStart;
        if (!moveRight)
        {
            FlipPlayer();
        }
    }

    void SyncAnimations()
    {
        anim.SetBool("jump", jump);
        anim.SetBool("grounded", grounded);
        anim.SetFloat("moveHor", Mathf.Abs(horMovement));
    }
}
